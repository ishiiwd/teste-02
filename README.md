# TESTE - API RESTFUL

Desenvolvido em PHP/Codeigniter e MySQL.

## Instalação

Para rodar o serviço, é necessário utilizar um servidor Apache, instalar o banco de dados no MySQL e editar o caminho no .htacess.

## Como testar

Após isso, poderá testar o serviço através do App POSTMAN:

* [POSTMAN](https://www.getpostman.com/apps)

Em todas as requisições é necessário fazer autenticação via HEADER com o token abaixo:

```header
token: TyByYXRvIHJldSBhIHJvcGEgZG8gcmVpIGRlIFJvbWE=
```

## GET

GET: http://localhost/api

O resultado é similar ao exemplo abaixo:

```GET
[
    {
        "id": "1",
        "name": "Jair Messias Bolsonaro",
        "email": "bolsonaro.2018@gmail.com"
    },
    {
        "id": "7",
        "name": "Marina Silva",
        "email": "marina.silva@gmail.com"
    },
    {
        "id": "3",
        "name": "Pedro Luiz Silva",
        "email": "pedro.luiz.silva@gmail.com"
    }
]
```

##POST

POST: http://localhost/api

Para envio os parametros deverão ser enviado no formato Json dentro do body:

```post
{
 	"nome" : "João da Silva",
 	"email" : "joao.silva@gmail.com"
 }
```

##PUT

PUT: http://localhost/api

Para atualização parametros deverão ser enviado no formato Json dentro do body:

```put
{
	"id" : "2",
	"nome" : "Marina Silva",
	"email" : "marina.silva@gmail.com"
}
```

##DELETE

DELETE: http://localhost/api

Para exclusão do cadastro, parametros deverão ser enviado no formato Json dentro do body:

```delete
{
	"id" : "7"
}
```

## Licença

Projeto exclusivamente desenvolvido para avaliação da Sennit Tecnologia.
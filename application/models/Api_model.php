<?php 

class Api_model extends CI_Model {
	
	function Articles_model()
	{
	  parent::CI_Model();
	}

	//Acesso
    function usuario($token=null)
    {

        $this->db->where('token', $token);
        $query = $this->db->get('usuarios');

        if($query->num_rows() == 1)
        {
            return true;
        }
    }

    //Cadastro
    function cadastro_listar($id=0)
    {
        if ($id) $this->db->where('id', $id);
        $this->db->order_by("nome", "ASC");
        $query = $this->db->get('cadastro');
        return $query->result();

    }

    function cadastro_add($data)
    {
        $this->db->insert('cadastro', $data);
    }

    function cadastro_up($data, $id=0)
    {
        $this->db->where('id', $id);
        $this->db->update('cadastro', $data);
    }

    function cadastro_del($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('cadastro');
    }

    //Autenticao do sistema
    function autenticacao()
    {
        $token  = $this->input->get_request_header('token', TRUE);

        if (!$token):
            http_response_code(203);
            exit;
        endif;

        if (!$this->usuario($token)):
            http_response_code(401);
            exit;
        endif;
    }

	/*********************************************************************/
	
	//Formata data e hora
	function dataHourToMysql($dt) {
		
		if ($dt=="00/00/0000") return '';
		
		$yr = strval(substr($dt,6,4));
		$mo = strval(substr($dt,3,2));
		$da = strval(substr($dt,0,2));
		$hr = strval(substr($dt,11,2));
		$mn = strval(substr($dt,14,2));	
		$sg = strval(substr($dt,17,2));	
		
		if (!is_numeric($da) and !is_numeric($mo) and !is_numeric($yr)){
			return '';
		}else{
			return $yr."-".$mo."-".$da.' '.$hr.':'.$mn.':'.$sg;
		}
	}

    function dataToMysql($dt) {

        if ($dt=="00/00/0000") return '';

        $yr = strval(substr($dt,6,4));
        $mo = strval(substr($dt,3,2));
        $da = strval(substr($dt,0,2));

        if (!is_numeric($da) and !is_numeric($mo) and !is_numeric($yr)){
            return '';
        }else{
            return $yr."-".$mo."-".$da;
        }
    }
	
	function mostraData($dt) {
		
		if ($dt=="0000-00-00") return '';
		$yr=strval(substr($dt,0,4));
		$mo=strval(substr($dt,5,2));
		$da=strval(substr($dt,8,2));
		return $da."/".$mo."/".$yr;
	}

	function mostraDataHora($dt) {
		
		if ($dt=="0000-00-00") return '';
		$yr=strval(substr($dt,0,4));
		$mo=strval(substr($dt,5,2));
		$da=strval(substr($dt,8,2));
		$hr=strval(substr($dt,11,2));
		$mn=strval(substr($dt,14,2));	
		$sg=strval(substr($dt,17,2));	
		return $da."/".$mo."/".$yr.' '.$hr.':'.$mn.':'.$sg;
	}

    function contarDias($data_inicial, $data_final)
    {
        // Usa a função strtotime() e pega o timestamp das duas datas:
        $time_inicial = strtotime($data_inicial);
        $time_final = strtotime($data_final);

        // Calcula a diferença de segundos entre as duas datas:
        $diferenca = $time_final - $time_inicial; // 19522800 segundos

        // Calcula a diferença de dias
        $dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias

        return $dias;
    }
		
	function removeAcentos($string, $slug = false) {
		
		$string = strtolower($string);
		
		// Código ASCII das vogais
		$ascii['a'] = range(224, 230);
		$ascii['e'] = range(232, 235);
		$ascii['i'] = range(236, 239);
		$ascii['o'] = array_merge(range(242, 246), array(240, 248));
		$ascii['u'] = range(249, 252);
		
		// Código ASCII dos outros caracteres
		$ascii['b'] = array(223);
		$ascii['c'] = array(231);
		$ascii['d'] = array(208);
		$ascii['n'] = array(241);
		$ascii['y'] = array(253, 255);
		
		foreach ($ascii as $key=>$item) {
			$acentos = '';
			foreach ($item AS $codigo) $acentos .= chr($codigo);
			$troca[$key] = '/['.$acentos.']/i';
		}
		
		$string = preg_replace(array_values($troca), array_keys($troca), $string);
		
		// Slug?
		if ($slug) {
			// Troca tudo que não for letra ou número por um caractere ($slug)
			$string = preg_replace('/[^a-z0-9]/i', $slug, $string);
			// Tira os caracteres ($slug) repetidos
			$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
			$string = trim($string, $slug);
		}
		
		return $string;
	}
	
	function criar_pasta($dir, $id)
	{
		//criar pasta
		$pasta = $dir.$id;
		
		if (!is_dir($pasta)):
			mkdir($pasta, 0777);
		endif;
		
		return $id;
	}
	
	function upper($str) {
		$LATIN_UC_CHARS = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ";
		$LATIN_LC_CHARS = "àáâãäåæçèéêëìíîïðñòóôõöøùúûüý";
		$str = strtr ($str, $LATIN_LC_CHARS, $LATIN_UC_CHARS);
		$str = strtoupper($str);
		return $str;
	}

    function filtrar($txt){
        /*$txt = str_replace("'", "\'", $txt);
        $txt = str_replace("\n", "<br>", $txt);
        $txt = str_replace(chr(10), "<br>", $txt);*/
        $txt = preg_replace("/\r\n|\r/", "<br />", $txt);
        $txt = trim($txt);
        return $txt;
    }

    function desfiltrar($txt){
        $txt = str_replace("\'", "'", $txt);
        $txt = str_replace("<br>", chr(10), $txt);
        return $txt;
    }

    function imgName($nome,$limite=0) {
        $or = array("#","@","!","$","%","¨","&","*"," ","á","à","ã","â","Á","À","Ã","Â","é","è","ê","É","È","Ê","í","ì","Í","Ì","ó","ò","õ","ô","Ó","Ò","Ô","Õ","ú","ù","Ú","Ù","Ç","ç");
        $sub = array("_","_","_","_","_","_","_","_","_","a","a","a","a","A","A","A","A","e","e","e","E","E","E","i","i","I","I","o","o","o","o","O","O","O","O","u","u","U","U","C","c");
        if($limite != 0):
            list($fileName, $ext) = explode(".",$nome);
            $nome = substr($fileName,0,$limite-4);
            $nome .= ".".$ext;
        endif;
        $new = str_replace($or,$sub,$nome);
        $new = $this->antiInjection(strtolower($new));
        return $new;
    }

    function antiInjection($str, $html=false){
        # Remove palavras suspeitas de injection.
        $str = preg_replace(preg_quote("/(\n|\r|%0a|%0d|Content-Type:|bcc:|to:|cc:|Autoreply:|from|select|insert|delete|where|drop table|show tables|#|[]|[|]|'|\*|--|\\\\)/"), "", $str);
        if($html == false):
            $str = strip_tags($str);  # Remove tags HTML e PHP.
            $str = trim($str);        # Remove espaços vazios.
        endif;

        $str = addslashes($str);  # Adiciona barras invertidas à uma string.
        return $str;
    }
		
}
?>
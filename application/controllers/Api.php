<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('api_model');

        //Valida o acesso
        $this->api_model->autenticacao();
    }

    public function index()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET'):
            header('Content-Type: application/json');

            $stream_clean   = $this->security->xss_clean($this->input->raw_input_stream);
            $resp           = json_decode($stream_clean);

            if ($rsCadastro = $this->api_model->cadastro_listar(isset($resp->id) and $resp->id ? $resp->id : 0)):

                $arrCadastro    = [];

                foreach ($rsCadastro as $row):
                    $arrCadastro[] = [
                        "id"        => $row->id,
                        "name"      => $row->nome,
                        "email"     => $row->email
                    ];
                endforeach;

                echo json_encode($arrCadastro);

            else:
                http_response_code(400);
            endif;

        else:
            http_response_code(400);
        endif;
    }

    public function post()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST'):
            $stream_clean   = $this->security->xss_clean($this->input->raw_input_stream);
            $resp           = json_decode($stream_clean);

            if ((isset($resp->nome) and $resp->nome) and (isset($resp->email) and $resp->email)):

                $data = [
                    'nome'    => $resp->nome,
                    'email'   => $resp->email
                ];
                $this->api_model->cadastro_add($data);

                http_response_code(200);

            else:
                http_response_code(404);
            endif;

        else:
            http_response_code(400);
        endif;
    }

    public function put()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'PUT'):
            $stream_clean   = $this->security->xss_clean($this->input->raw_input_stream);
            $resp           = json_decode($stream_clean);

            if ((isset($resp->nome) and $resp->nome) and (isset($resp->email) and $resp->email)):

                $data = [
                    'nome'    => $resp->nome,
                    'email'   => $resp->email
                ];
                (isset($resp->id) and $resp->id) ? $this->api_model->cadastro_up($data, $resp->id) : $this->api_model->cadastro_add($data);

                http_response_code(200);

            else:
                http_response_code(404);
            endif;
        else:
            http_response_code(400);
        endif;
    }

    public function delete()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE'):
            $stream_clean   = $this->security->xss_clean($this->input->raw_input_stream);
            $resp           = json_decode($stream_clean);

            if (isset($resp->id) and $resp->id):

                $this->api_model->cadastro_del($resp->id);
                http_response_code(200);

            else:
                http_response_code(404);
            endif;
        else:
            http_response_code(400);
        endif;
    }

    public function options()
    {

    }

}
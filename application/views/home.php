<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Dia</title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="icon" type="image/x-icon" href="../icone_dia.ico" />
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script>
        function selSelectBox(object,valor) {
            for (var i=0; i<object.length; i++) {
                if (object.options[i].value === valor){
                    object.options[i].selected=true;
                }
            }
        }
    </script>

    <!---google analytcs-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-63382111-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>

<!---<div class="navbar navbar-inverse">-->
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>-->

        <div class="col-md-12 text-center"><a class="" href="http://dia.com.br" target="_blank"><img src="<?=base_url()?>assets/img/logo-dia-atual.png" alt="Dia" /></a></div>

    </div><!-- /.container -->
</div><!-- /.navbar -->

<!-- HEADER
=================================-->
<!--
<div class="container banner">
      <div class="row">
        <div class="col col-lg-12 col-sm-12">
          <img src="img/banner-diamkt-cerveja.jpg" alt="" class="img-responsive" />
        </div>
      </div>
</div>
<!-- /header container-->

<!--<script>
    $('.carousel').carousel({
        interval: 3000
    });
</script>-->
<div class="container" style="height:80px;">

</div>




<!-- CONTENT
=================================-->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <h1>Encontre a loja mais próxima de você</h1></div>
            <div class="col-md-12">

                <!--==============================row1=================================-->
                <div class="row_2">
                    <div class="container">

                        <div class="col-md-3 col-xs-12"></div>

                        <div class="col-md-6 col-xs-12">
                            <form id="retorno" name="form" action="<?=$action?>" method="post">
                                <input type="hidden" name="estado" value="<?=$pestado?>">
                                <div class="box-form2">
                                    <div style="margin-bottom:10px;">
                                        <select name="cidade" id="cidade">
                                            <option value="">Selecione sua cidade</option>
                                            <?php if(isset($arr_cidades)) : foreach( $arr_cidades as $row): ?>
                                                <option value="<?=$row->cidade;?>"><?=$row->cidade;?></option>
                                            <?php  endforeach; endif; ?>
                                        </select>
                                        <?=$cidade ? "<script>selSelectBox(document.form.cidade,'$cidade');</script>" : "";?>
                                    </div>

                                    <div style="margin-bottom:10px;">
                                        <select name="bairro" id="bairro">
                                            <option value="">Selecione o seu bairro</option>
                                        </select>
                                    </div>
                                    <input name="Procurar" id="btn_submit" type="submit" value="Procurar" class="submit">
                                </div>
                            </form>
                        </div>

                        <div class="col-md-3 col-xs-12"></div>
                    </div>
                </div>
                <!---row-2-->

                <script>
                    $(document).ready(function () {

                        var http = (window.location.protocol=='https:') ? 'https://' : 'http://';
                        var base_host = http+window.location.host+'/flv/index/';

                        <?php if (isset($bairro) and $bairro>0):?>
                        //buscar bairro
                        $('#bairro').html('<option>Aguarde, consultando...</option>');

                        //load bairro
                        $.post(base_host+'bairros', {
                                acao 	    : 'bairros',
                                estado	    : '<?=$pestado?>',
                                perecivel	: '<?=$perecivel?>',
                                cidade 	    : '<?=$cidade?>',
                                bairro	    : <?=$bairro?>,
                            },
                            //retorna resultado
                            function(data) {
                                $('#bairro').html(data);
                            });
                        <?php endif; ?>

                        $('#cidade').change(function(){

                            //buscar bairro
                            $('#bairro').html('<option>Aguarde, consultando...</option>');

                            //load bairro
                            $.post(base_host+'bairros', {
                                    acao 		: 'bairros',
                                    estado	    : '<?=$pestado?>',
                                    perecivel	: '<?=$perecivel?>',
                                    cidade 	    : $(this).val()
                                },
                                //retorna resultado
                                function(data) {
                                    $('#bairro').html(data);
                                });

                            return false;

                        });

                    });

                </script>

                <?php if ($acao=='loja'): ?>
                    <div class="row_2 lojas">

                        <!--<div class="container boder_lojas">
                            <p class="txtLojas2" style="color:#fff !important;" id="lojas">LOJAS</p>
                        </div>-->

                        <section id="mapa-loja">
                            <div class="container">
                                <div class="infoloja">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="titlelojabox">
                                                <div class="row">
                                                    <div class="col-md-6" style="height:80px">
                                                        <h3><?=$cidade;?></h3>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="tipolojatexto"><?=$legenda?></span> <br>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="boxinfo">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5><i class="fa fa-map-marker"></i> Endereço</h5>
                                                        <p><?=$endereco;?>, <?=$numero ?> <br> <?=$bairro ?> <br> <?=$cidade; ?> - <?=$estado; ?></p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <h5><i class="fa fa-clock-o"></i> Horário</h5>
                                                        <p>Segunda a sábado <?=$util_abre ?> às <?=$util_fecha ?> / Domingos <?php
                                                            if($dom_abre == '00:00' && $dom_fecha == '00:00'):
                                                                echo 'N&atilde; abre';
                                                            else:
                                                                echo $dom_abre .' às '. $dom_fecha;
                                                            endif;
                                                            ?> / Feriados <?php
                                                            if($fer_abre == '00:00' && $fer_fecha == '00:00'):
                                                                echo 'N&atilde; abre';
                                                            else:
                                                                echo $fer_abre .' às '. $fer_fecha;
                                                            endif;
                                                            ?></span>
                                                        </p>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="map"></div>
                                            <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false.js"></script>
                                            <script type='text/javascript'>
                                                //<![CDATA[
                                                var map;
                                                var global_markers = [];
                                                var markers = [[<?=$lat;?>,<?=$lng;?>, 'DIA - <?=$bairro?>']];

                                                var infowindow = new google.maps.InfoWindow({});

                                                function initialize() {
                                                    geocoder = new google.maps.Geocoder();
                                                    var latlng = new google.maps.LatLng(<?=$lat;?>, <?=$lng;?>);
                                                    var myOptions = {
                                                        zoom: 10,
                                                        center: latlng,
                                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    }
                                                    map = new google.maps.Map(document.getElementById("map"), myOptions);
                                                    addMarker();
                                                }

                                                function addMarker() {
                                                    for (var i = 0; i < markers.length; i++) {
                                                        // obtain the attribues of each marker
                                                        var lat = parseFloat(markers[i][0]);
                                                        var lng = parseFloat(markers[i][1]);
                                                        var trailhead_name = markers[i][2];

                                                        var myLatlng = new google.maps.LatLng(lat, lng);
                                                        var image = '<?=base_url()?>assets/img/map_marker.png';

                                                        var marker = new google.maps.Marker({
                                                            position: myLatlng,
                                                            map: map,
                                                            title: trailhead_name,
                                                            icon: image
                                                        });

                                                        marker['infowindow'] = trailhead_name;

                                                        global_markers[i] = marker;

                                                        google.maps.event.addListener(global_markers[i], 'click', function() {
                                                            infowindow.setContent(this['infowindow']);
                                                            infowindow.open(map, this);
                                                        });
                                                    }
                                                }
                                                window.onload = initialize;
                                                //]]>
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>

                    </div>
                <? endif; ?>
                <br>
                <br>
                <br>

            </div>
        </div>
    </div>

</div>

<footer>
    <section class="prefooter">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-6">
                    <div class="superdia">
                        <h6><a href="http://dia.com.br/" target="_blank">Supermercado DIA. Economia de Verdade.</a></h6>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <div class="grupodia">
                        <ul>
                            <li><a href="http://www.diacorporate.com/en/" target="_blank">DIA Corporate</a></li>
                            <li><a href="http://www.franquiadia.com.br/" target="_blank">DIA Franquias</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="adressbottom text-center">
        <div class="container"  itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="row">
                <div class="col-md-12">
                    <span class="" itemprop="name">DIA Brasil Sociedade LTDA </span> <span> | CNPJ: 03.476.811/0001-51 | </span><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">Avenida Doutor Cardoso de Melo, 1855. 1° andar.</span> <span itemprop="addressLocality"> Vila Olímpia. São Paulo </span> - <span itemprop="addressRegion">SP</span>  <span> CEP: 04548-005</span>
                </div>
            </div>
        </div>
    </section>

</footer>


<!-- /CONTENT ============-->
<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
</body>
</html>